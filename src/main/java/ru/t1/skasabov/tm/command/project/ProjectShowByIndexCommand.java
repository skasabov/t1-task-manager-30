package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-show-by-index";

    @NotNull private static final String DESCRIPTION = "Display project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}
