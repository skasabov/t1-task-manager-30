package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.model.User;

@NoArgsConstructor
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "view-user-profile";

    @NotNull private static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
