package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.model.User;

@NoArgsConstructor
public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
